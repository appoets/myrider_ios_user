//
//  SeviceSelectionView.swift
//  GoJekUser
//
//  Created by Ansar on 26/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class ServiceSelectionView: UIView {
    
    @IBOutlet weak var serviceCollectionView: UICollectionView!
    @IBOutlet weak var serviceTitleLabel: UILabel!
    @IBOutlet weak var cardOrCashLabel:UILabel!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var getPricingButton: UIButton!
    @IBOutlet weak var paymentImage:UIImageView!
    @IBOutlet weak var dottedLineView:UIView!
    @IBOutlet weak var dailyButton: UIButton!
    @IBOutlet weak var rentalsButton: UIButton!
    @IBOutlet weak var outstationButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timerButton: UIButton!
    @IBOutlet weak var packageCollectionView: UICollectionView!
    @IBOutlet weak var oneWayTripView: UIView!
    @IBOutlet weak var oneWayTripTitleLabel: UILabel!
    @IBOutlet weak var oneWayTripSubTitleLabel: UILabel!
    @IBOutlet weak var roundTripView: UIView!
    @IBOutlet weak var roundTripTitleLabel: UILabel!
    @IBOutlet weak var roundTripSubTitleLabel: UILabel!
    @IBOutlet weak var oneWayTripButton: UIButton!
    @IBOutlet weak var roundTripButton: UIButton!
    @IBOutlet weak var bookingForLabel: UILabel!
    @IBOutlet weak var bookingTimeLabel: UILabel!
    @IBOutlet weak var startSelectButton: UIButton!
    @IBOutlet weak var endSelectButton: UIButton!
    @IBOutlet weak var oneWaytickImageView: UIImageView!
    @IBOutlet weak var roundtickImageView: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var packageView: UIView!
    @IBOutlet weak var tripView: UIStackView!
    @IBOutlet weak var bookingView: UIView!
    @IBOutlet weak var tripTitleView: UIView!
    @IBOutlet weak var selectTripLabel: UILabel!

    
    var paymentMode:PaymentType = .CASH {
        didSet {
            paymentImage.image = paymentMode.image
            cardOrCashLabel.text = paymentMode.rawValue
        }
    }
    var taxiTrip:TaxiTrip? = .oneWay
    var taxitype:TaxiServiceType = .ride
    
    private var selectedRow = 0
    private var selectedPackageRow = 0
    var selectedCard: CardResponseData?

    
    var serviceTypeArr = [TaxiConstant.hatchBag,TaxiConstant.sedan,TaxiConstant.suv]
    
    var tapService:((Services)->Void)?

    var tapGetPricing:((Services,Rental_packages?)->Void)?
    
    var tapBookingDate:((TaxiTrip,Int)->Void)?
    
    var serviceDetails:[Services] = [] {
        didSet{
            serviceCollectionView.reloadInMainThread()
            if serviceDetails.count != 0 {
                packageArr = serviceDetails[selectedRow].price_details?.rental_packages ?? []
            }

        }
    }
    
    var packageArr:[Rental_packages] = [] {
        didSet{
            packageCollectionView.reloadInMainThread()
        }
    }
    
    // clouser for payment
    var paymentChangeClick: ((PaymentType,CardResponseData)-> Void)?
    
    var tapPackage: ((TaxiServiceType)-> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialLoads()
    }

}

//MARK: - Methods

extension ServiceSelectionView {
    
    private func initialLoads() {
        setColorAndBorder()
        setFont()
        oneWayTripView.setCornerRadiuswithValue(value: 8)
        roundTripView.setCornerRadiuswithValue(value: 8)
        topView.backgroundColor = .veryLightGray
        topView.setCornerRadiuswithValue(value: 10)
        serviceCollectionView.delegate = self
        serviceCollectionView.dataSource = self
        serviceCollectionView.register(UINib(nibName: TaxiConstant.ServiceTypeCell,bundle: nil), forCellWithReuseIdentifier: TaxiConstant.ServiceTypeCell)
        packageCollectionView.delegate = self
        packageCollectionView.dataSource = self
        startSelectButton.addTarget(self, action: #selector(onSelectDate), for: .touchUpInside)
        endSelectButton.addTarget(self, action: #selector(onSelectDate), for: .touchUpInside)
        startSelectButton.tag = 0
        endSelectButton.tag = 1
        packageCollectionView.register(UINib(nibName: TaxiConstant.PackageCollectionCell,bundle: nil), forCellWithReuseIdentifier: TaxiConstant.PackageCollectionCell)
        getPricingButton.addTarget(self, action: #selector(onClickGetPricing), for: .touchUpInside)
        changeButton.addTarget(self, action: #selector(changePaymentButtonTapped), for: .touchUpInside)
        paymentMode = .CASH
        localize()
        setDarkMode()
        dailyButton.addTarget(self, action: #selector(onClickServiceType(_:)), for: .touchUpInside)
        rentalsButton.addTarget(self, action: #selector(onClickServiceType(_:)), for: .touchUpInside)
        outstationButton.addTarget(self, action: #selector(onClickServiceType(_:)), for: .touchUpInside)
        roundTripButton.addTarget(self, action: #selector(canCheckTripButtonAction(_:)), for: .touchUpInside)
        oneWayTripButton.addTarget(self, action: #selector(canCheckTripButtonAction(_:)), for: .touchUpInside)
        packageView.isHidden = true
        tripTitleView.isHidden = true
        packageCollectionView.isHidden = true
        tripView.isHidden = true
        bookingView.isHidden = true
        bookingTimeLabel.isHidden = true
        endSelectButton.isHidden = true
        timerButton.setImage(UIImage(named: TaxiConstant.ic_time)?.imageTintColor(color1: .taxiColor).resizeImage(newWidth: 20), for: .normal)
        oneWaytickImageView.image = UIImage(named: TaxiConstant.ic_check)
        roundtickImageView.image = nil
    }
    
    private func setDarkMode(){
        self.backgroundColor = .boxColor
        self.serviceCollectionView.backgroundColor = .boxColor
    }
    
    private func localize() {
        serviceTitleLabel.text = TaxiConstant.dailyRide.localized
        dailyButton.setTitle(TaxiConstant.daily.localized, for: .normal)
        rentalsButton.setTitle(TaxiConstant.rentals.localized, for: .normal)
        outstationButton.setTitle(TaxiConstant.outstation.localized, for: .normal)
        changeButton.setTitle(Constant.change.localized.uppercased(), for: .normal)
        getPricingButton.setTitle(TaxiConstant.getPricing.localized.uppercased().uppercased(), for: .normal)
        oneWayTripTitleLabel.text = TaxiConstant.oneWay.localized
        roundTripTitleLabel.text = TaxiConstant.roundTrip.localized
        roundTripSubTitleLabel.text = TaxiConstant.carReturn.localized
        oneWayTripSubTitleLabel.text = TaxiConstant.getdropoff.localized
        bookingForLabel.text = TaxiConstant.bookingFor.localized
        startSelectButton.setTitle(TaxiConstant.startTime.uppercased(), for: .normal)
        endSelectButton.setTitle(TaxiConstant.endTime.uppercased(), for: .normal)
    }
    
    @objc func onSelectDate(_ sender:UIButton){
        if taxiTrip?.currentTrip == TaxiTrip.oneWay.rawValue {
            self.tapBookingDate!(TaxiTrip(rawValue: taxiTrip!.currentTrip) ?? .oneWay,0)

        }else{
            self.tapBookingDate!(TaxiTrip(rawValue: taxiTrip!.currentTrip) ?? .roundway,sender.tag)
        }
    }
    
    @objc func onSelectendDate(){
        if taxiTrip?.currentTrip == TaxiTrip.oneWay.rawValue {
            self.tapBookingDate!(TaxiTrip(rawValue: taxiTrip!.currentTrip) ?? .oneWay,0)

        }else{
            self.tapBookingDate!(TaxiTrip(rawValue: taxiTrip!.currentTrip) ?? .roundway,2)
        }
    }
    
    private func setFont() {
        changeButton.titleLabel?.font = .setCustomFont(name: .bold, size: .x12)
        cardOrCashLabel.font = .setCustomFont(name: .medium, size: .x12)
        getPricingButton.titleLabel?.font = .setCustomFont(name: .medium, size: .x18)
        serviceTitleLabel.font = .setCustomFont(name: .medium, size: .x20)
        dailyButton.titleLabel?.font = .setCustomFont(name: .medium, size: .x12)
        rentalsButton.titleLabel?.font = .setCustomFont(name: .medium, size: .x12)
        outstationButton.titleLabel?.font = .setCustomFont(name: .medium, size: .x12)
        titleLabel.font = .setCustomFont(name: .medium, size: .x14)
        oneWayTripSubTitleLabel.font = .setCustomFont(name: .medium, size: .x12)
        oneWayTripTitleLabel.font = .setCustomFont(name: .medium, size: .x14)
        roundTripSubTitleLabel.font = .setCustomFont(name: .medium, size: .x12)
        roundTripTitleLabel.font = .setCustomFont(name: .medium, size: .x14)
        bookingForLabel.font = .setCustomFont(name: .medium, size: .x12)
        bookingTimeLabel.font = .setCustomFont(name: .medium, size: .x12)
        endSelectButton.titleLabel?.font = .setCustomFont(name: .medium, size: .x12)
        startSelectButton.titleLabel?.font = .setCustomFont(name: .bold, size: .x12)
        endSelectButton.titleLabel?.font = .setCustomFont(name: .bold, size: .x12)
    }
    
    private func setColorAndBorder() {
        dailyButton.backgroundColor = .taxiColor
        rentalsButton.backgroundColor = .veryLightGray
        outstationButton.backgroundColor = .veryLightGray
        serviceTitleLabel.textColor = .taxiColor
        getPricingButton.backgroundColor = .taxiColor
        changeButton.backgroundColor = UIColor.taxiColor.withAlphaComponent(0.2)
        changeButton.layer.borderColor = UIColor.taxiColor.cgColor
        dailyButton.setTitleColor(.white, for: .normal)
        rentalsButton.setTitleColor(.black, for: .normal)
        outstationButton.setTitleColor(.black, for: .normal)
        changeButton.textColor(color: .taxiColor)
        changeButton.borderColor = .taxiColor
        changeButton.borderLineWidth = 1.0
        changeButton.cornerRadius = 5.0
        getPricingButton.cornerRadius = 5.0
        roundTripView.backgroundColor = .veryLightGray
        oneWayTripView.backgroundColor = .taxiColor
        oneWayTripTitleLabel.textColor = .white
        oneWayTripSubTitleLabel.textColor = .white
        bookingTimeLabel.textColor = .taxiColor
        startSelectButton.setTitleColor(.appPrimaryColor, for: .normal)
        endSelectButton.setTitleColor(.appPrimaryColor, for: .normal)
    }
    
    @objc func canCheckTripButtonAction(_ sender:UIButton){
        if sender.tag == 0 {
            roundTripView.backgroundColor = .veryLightGray
            oneWayTripView.backgroundColor = .taxiColor
            oneWayTripTitleLabel.textColor = .white
            oneWayTripSubTitleLabel.textColor = .white
            roundTripTitleLabel.textColor = .black
            roundTripSubTitleLabel.textColor = .black
            bookingTimeLabel.isHidden = true
            endSelectButton.isHidden = true
            taxiTrip = .oneWay
            oneWaytickImageView.image = UIImage(named: TaxiConstant.ic_check)
            roundtickImageView.image = nil
        }else{
            roundTripView.backgroundColor = .taxiColor
            oneWayTripView.backgroundColor = .veryLightGray
            oneWayTripTitleLabel.textColor = .black
            oneWayTripSubTitleLabel.textColor = .black
            roundTripTitleLabel.textColor = .white
            bookingTimeLabel.isHidden = false
            endSelectButton.isHidden = false
            roundTripSubTitleLabel.textColor = .white
            taxiTrip = .roundway
            oneWaytickImageView.image = nil
            roundtickImageView.image = UIImage(named: TaxiConstant.ic_check)
        }
    }
    
    override func layoutSubviews() {
        dottedLineView.addSingleLineDash(color: .black, width: 0.8)
    }
    
    @objc func onClickGetPricing() {
        guard selectedRow > -1 else {
            ToastManager.show(title: TaxiConstant.selectServiceType.localized, state: .error)
            return
        }
        if  taxitype == .rental {
            guard selectedPackageRow > -1 else {
                ToastManager.show(title: TaxiConstant.selectPackageType.localized, state: .error)
                return
            }
            if packageArr.count > 0 {
                tapGetPricing?(serviceDetails[selectedRow],packageArr[selectedPackageRow])
            }else{
                ToastManager.show(title: TaxiConstant.selectPackageType.localized, state: .error)
                return
            }
        }
       else if  taxitype == .outstation || taxitype == .ride{
            tapGetPricing?(serviceDetails[selectedRow],nil)
        }else{
            if packageArr.count > 0 {
            tapGetPricing?(serviceDetails[selectedRow],packageArr[selectedPackageRow])
            }else{
                ToastManager.show(title: TaxiConstant.selectPackageType.localized, state: .error)
                return
            }
        }
    }
    
    // change payment before sending request
    @objc func changePaymentButtonTapped() {
        let paymentVC = AccountRouter.accountStoryboard.instantiateViewController(withIdentifier: AccountConstant.PaymentSelectViewController) as! PaymentSelectViewController
        paymentVC.isChangePayment = true
        paymentVC.selectCardId = selectedCard?.card_id ?? ""
        paymentVC.onClickPayment = { [weak self] (type,cardEntity) in
            guard let self = self else {
                return
            }
            self.paymentMode = type
            if type == .CARD {
                self.selectedCard = cardEntity
                self.cardOrCashLabel.text = Constant.cardPrefix + (cardEntity?.last_four ?? "")
            }else{
                self.cardOrCashLabel.text = type.rawValue
            }
            self.paymentChangeClick?(type,cardEntity ?? CardResponseData())
        }
        UIApplication.topViewController()?.navigationController?.pushViewController(paymentVC, animated: true)
    }
    
    @objc func onClickServiceType(_ sender:UIButton){
        selectedRow = 0
        if sender.tag == 0 {
            dailyButton.backgroundColor = .taxiColor
            rentalsButton.backgroundColor = .veryLightGray
            outstationButton.backgroundColor = .veryLightGray
            sender.setTitleColor(.white, for: .normal)
            rentalsButton.setTitleColor(.black, for: .normal)
            outstationButton.setTitleColor(.black, for: .normal)
            packageView.isHidden = true
            tripTitleView.isHidden = true
            packageCollectionView.isHidden = true
            tripView.isHidden = true
            bookingView.isHidden = true
            self.taxitype = .ride
            self.tapPackage?(TaxiServiceType.ride)
        }else if sender.tag == 1 {
            rentalsButton.backgroundColor = .taxiColor
            dailyButton.backgroundColor = .veryLightGray
            outstationButton.backgroundColor = .veryLightGray
            sender.setTitleColor(.white, for: .normal)
            dailyButton.setTitleColor(.black, for: .normal)
            outstationButton.setTitleColor(.black, for: .normal)
            titleLabel.text = TaxiConstant.selectPackage.localized
            packageView.isHidden = false
            tripTitleView.isHidden = true
            packageCollectionView.isHidden = false
            tripView.isHidden = true
            bookingView.isHidden = true
            self.taxitype = .rental
            self.tapPackage?(TaxiServiceType.rental)
        }else if sender.tag == 2 {
            outstationButton.backgroundColor = .taxiColor
            dailyButton.backgroundColor = .veryLightGray
            rentalsButton.backgroundColor = .veryLightGray
            sender.setTitleColor(.white, for: .normal)
            rentalsButton.setTitleColor(.black, for: .normal)
            dailyButton.setTitleColor(.black, for: .normal)
            selectTripLabel.text = TaxiConstant.selectTrip.localized
            packageView.isHidden = true
            tripTitleView.isHidden = false
            packageCollectionView.isHidden = true
            tripView.isHidden = false
            bookingView.isHidden = false
            self.taxitype = .outstation
            self.tapPackage?(TaxiServiceType.outstation)
        }
    }

}

//MARK: - Collectionview delegate & datasource
extension ServiceSelectionView: UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView.tag == 0 {
            return 1
        }else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        if collectionView.tag == 0 {
            count = serviceDetails.count
        }else{
           count = packageArr.count
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 {
            if let cell:ServiceTypeCell = serviceCollectionView.dequeueReusableCell(withReuseIdentifier: TaxiConstant.ServiceTypeCell, for: indexPath) as? ServiceTypeCell,indexPath.row <= serviceDetails.count {
                cell.isCurrentService = selectedRow == indexPath.row ? true : false
                cell.serviceNameLabel.text = serviceDetails[indexPath.row].vehicle_name
                cell.serviceImage.sd_setImage(with:  URL(string: self.serviceDetails[indexPath.row].vehicle_image ?? ""), placeholderImage:#imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    // Perform operation.
                    if (error != nil) {
                        // Failed to load image
                        cell.serviceImage.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                    } else {
                        // Successful in loading image
                        cell.serviceImage.image = image
                    }
                })
                cell.etaLabel.text = serviceDetails[indexPath.row].estimated_time
                cell.isCurrentService = selectedRow == indexPath.row
                return cell
                
            }
        }else{
            let cell:PackageCollectionCell = (packageCollectionView.dequeueReusableCell(withReuseIdentifier: TaxiConstant.PackageCollectionCell, for: indexPath) as? PackageCollectionCell)!
            cell.isCurrentPackage = selectedPackageRow == indexPath.row ? true : false
            let dict = packageArr[indexPath.row]
            cell.titleLabel.text = (dict.hour ?? "") + "hr"
            cell.subTitleLabel.text = (dict.km ?? "") + "Km"
            cell.isCurrentPackage = selectedPackageRow == indexPath.row
            return cell
        }
        return UICollectionViewCell()
    }
}

extension ServiceSelectionView:UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0 {
            if let cell:ServiceTypeCell = serviceCollectionView.cellForItem(at: indexPath) as? ServiceTypeCell, indexPath.row < serviceDetails.count {
                selectedPackageRow = 0
                cell.isCurrentService = false
                if selectedRow == indexPath.row {
                    tapService?(serviceDetails[selectedRow]) //Double tap service and get rate card
                }
                selectedRow = indexPath.row
                packageArr = serviceDetails[indexPath.row].price_details?.rental_packages ?? []
                packageCollectionView.reloadInMainThread()
                serviceCollectionView.reloadInMainThread()
            }
        }else{
            if let cell:PackageCollectionCell = packageCollectionView.cellForItem(at: indexPath) as? PackageCollectionCell {
            cell.isCurrentPackage = false
                selectedPackageRow = indexPath.row
                packageCollectionView.reloadInMainThread()
            }
        }
    }
}

extension ServiceSelectionView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0 {
            let width = (collectionView.frame.width/3)
            let height = collectionView.frame.height
            return CGSize(width: width, height: height)
        }else{
            let height = collectionView.frame.height
            return CGSize(width: 80, height: height)
        }
        
    }
}
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
